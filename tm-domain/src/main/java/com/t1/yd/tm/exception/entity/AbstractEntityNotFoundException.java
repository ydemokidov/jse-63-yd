package com.t1.yd.tm.exception.entity;

import com.t1.yd.tm.exception.AbstractException;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

@NoArgsConstructor
public class AbstractEntityNotFoundException extends AbstractException {

    public AbstractEntityNotFoundException(@NotNull String message) {
        super(message);
    }

    public AbstractEntityNotFoundException(@NotNull String message, @NotNull Throwable cause) {
        super(message, cause);
    }

    public AbstractEntityNotFoundException(@NotNull Throwable cause) {
        super(cause);
    }

    public AbstractEntityNotFoundException(@NotNull String message, @NotNull Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
