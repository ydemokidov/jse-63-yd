package com.t1.yd.tm.repository.model;

import com.t1.yd.tm.model.Session;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

@Repository
@Scope("prototype")
public interface SessionJpaRepository extends AbstractJpaUserOwnedRepository<Session> {
}
