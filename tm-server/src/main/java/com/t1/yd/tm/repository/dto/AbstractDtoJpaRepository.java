package com.t1.yd.tm.repository.dto;

import com.t1.yd.tm.dto.model.AbstractEntityDTO;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

@NoRepositoryBean
public interface AbstractDtoJpaRepository<E extends AbstractEntityDTO> extends JpaRepository<E, String> {

    @Override
    List<E> findAll(@NotNull Sort sort);

}
