package com.t1.yd.tm.repository;

import com.t1.yd.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public enum TaskRepository {

    INSTANCE;
    @NotNull
    private final Map<String, Task> tasks = new LinkedHashMap<>();

    {
        add(new Task("First Task"));
        add(new Task("Second Task"));
        add(new Task("Third Task"));
    }

    public void create() {
        add(new Task("Task" + System.currentTimeMillis()));
    }

    public void add(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

    public void save(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

    @NotNull
    public Collection<Task> findAll() {
        return tasks.values();
    }

    @Nullable
    public Task findById(@NotNull final String id) {
        return tasks.get(id);
    }

    public void removeById(@NotNull final String id) {
        tasks.remove(id);
    }

}

