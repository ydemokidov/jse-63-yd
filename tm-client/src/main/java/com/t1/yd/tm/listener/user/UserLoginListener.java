package com.t1.yd.tm.listener.user;

import com.t1.yd.tm.api.endpoint.IAuthEndpoint;
import com.t1.yd.tm.api.endpoint.IUserEndpoint;
import com.t1.yd.tm.api.service.ITokenService;
import com.t1.yd.tm.dto.request.user.UserLoginRequest;
import com.t1.yd.tm.dto.response.user.UserLoginResponse;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.event.ConsoleEvent;
import com.t1.yd.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class UserLoginListener extends AbstractUserListener {

    @NotNull
    private final String name = "user_login";
    @NotNull
    private final String description = "User login";

    @Autowired
    public UserLoginListener(@NotNull final ITokenService tokenService,
                             @NotNull final IUserEndpoint userEndpointClient,
                             @NotNull final IAuthEndpoint authEndpointClient) {
        super(tokenService, userEndpointClient, authEndpointClient);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

    @Override
    @EventListener(condition = "@userLoginListener.getName()==#consoleEvent.name")
    public void handle(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[USER LOGIN]");
        System.out.println("[ENTER LOGIN:]");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD:]");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserLoginRequest request = new UserLoginRequest();
        request.setLogin(login);
        request.setPassword(password);

        @NotNull final UserLoginResponse response = getAuthEndpoint().login(request);
        @Nullable final String token = response.getToken();
        setToken(token);
    }

    @NotNull
    @Override
    public String getName() {
        return name;
    }

    @NotNull
    @Override
    public String getDescription() {
        return description;
    }

}
