package com.t1.yd.tm.servlet;

import com.t1.yd.tm.repository.ProjectRepository;
import org.jetbrains.annotations.NotNull;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/project/delete/*")
public class ProjectDeleteServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        @NotNull final String id = req.getParameter("id");
        ProjectRepository.INSTANCE.removeById(id);
        resp.sendRedirect("/projects");
    }

}
