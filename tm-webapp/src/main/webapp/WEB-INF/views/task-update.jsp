<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/header.jsp"/>

<h3>TASK EDIT</h3>

<form action="/task/update?id=${task.id}" method="POST">
    <input type="hidden" name="id" value="${task.id}"/>
    <p>
    <div>Name:</div>
    <div><input type="text" name="name" value="${task.name}"/></div>
    </p>
    <p>
    <div>Description:</div>
    <div><input type="text" name="description" value="${task.description}"/></div>
    </p>
    <p>
    <div>Status:</div>
    <select name="status">
        <c:forEach var="status" items="${statusValues}">
            <option
                    <c:if test="${task.status == status}">selected="selected"</c:if>
                    value="${status}">${status.displayName}</option>
        </c:forEach>
    </select>
    </p>
    <p>
    <div>Created:</div>
    <div><input type="date" name="created"
                value="<fmt:parseDate value="${task.created}" pattern="yyyy-MM-dd" var="taskCreatedStr" type="date"/>
        <fmt:formatDate pattern="yyyy-MM-dd" value="${taskCreatedStr}"/>"/>"/>
    </div>
    </p>
    <div>Project ID:</div>
    <select name="projectId">
        <option>-----</option>
        <c:forEach var="project" items="${projects}">
            <option
                    <c:if test="${project.id == task.projectId}">selected="selected"</c:if>
                    value="${project.id}">${project.name}</option>
        </c:forEach>
    </select>
    </p>
    <button type="submit" style="padding-top: 20px;">SAVE</button>
</form>

<jsp:include page="../include/footer.jsp"/>
