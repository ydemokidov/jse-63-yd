package com.t1.yd.tm.repository;

import com.t1.yd.tm.model.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public enum ProjectRepository {

    INSTANCE;
    @NotNull
    private final Map<String, Project> projects = new LinkedHashMap<>();

    {
        add(new Project("First Project"));
        add(new Project("Second Project"));
        add(new Project("Third Project"));
    }

    public void create() {
        add(new Project("Project" + System.currentTimeMillis()));
    }

    public void add(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

    public void save(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

    @NotNull
    public Collection<Project> findAll() {
        return projects.values();
    }

    @Nullable
    public Project findById(@NotNull final String id) {
        return projects.get(id);
    }

    public void removeById(@NotNull final String id) {
        projects.remove(id);
    }

}

