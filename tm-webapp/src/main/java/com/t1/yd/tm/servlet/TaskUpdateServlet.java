package com.t1.yd.tm.servlet;

import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.model.Task;
import com.t1.yd.tm.repository.ProjectRepository;
import com.t1.yd.tm.repository.TaskRepository;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@WebServlet("/task/update/*")
public class TaskUpdateServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final String id = req.getParameter("id");
        @Nullable final Task task = TaskRepository.INSTANCE.findById(id);
        req.setAttribute("task", task);
        req.setAttribute("statusValues", Status.values());
        req.setAttribute("projects", ProjectRepository.INSTANCE.findAll());
        req.getRequestDispatcher("/WEB-INF/views/task-update.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        @NotNull final String id = req.getParameter("id");
        @NotNull final String name = req.getParameter("name");
        @NotNull final String description = req.getParameter("description");
        @NotNull final String statusValue = req.getParameter("status");
        @NotNull final Status status = Status.valueOf(statusValue);
        @NotNull final String createdValue = req.getParameter("created");
        @NotNull final String projectId = req.getParameter("projectId");

        @NotNull final Task task = new Task();
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        task.setStatus(status);
        if (!createdValue.isEmpty())
            task.setCreated(LocalDate.parse(createdValue, DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        else task.setCreated(null);
        task.setProjectId(projectId);
        TaskRepository.INSTANCE.save(task);
        resp.sendRedirect("/tasks");
    }

}
