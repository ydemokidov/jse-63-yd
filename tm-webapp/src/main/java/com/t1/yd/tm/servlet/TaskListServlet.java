package com.t1.yd.tm.servlet;

import com.t1.yd.tm.repository.ProjectRepository;
import com.t1.yd.tm.repository.TaskRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/tasks")
public class TaskListServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("tasks", TaskRepository.INSTANCE.findAll());
        req.setAttribute("projects", ProjectRepository.INSTANCE.findAll());
        req.getRequestDispatcher("/WEB-INF/views/task-list.jsp").forward(req, resp);
    }
}
