package com.t1.yd.tm.service;

import com.jcabi.manifests.Manifests;
import com.t1.yd.tm.api.service.IPropertyService;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

@Service
@Getter
@Setter
@PropertySource("classpath:application.properties")
public class PropertyService implements IPropertyService {

    @Value("${database.server}")
    private String dbServer;

    @Value("${server.host}")
    private String host;

    @Value("${server.port}")
    private Integer serverPort;

    @Value("${session.key}")
    private String sessionKey;

    @Value("${session.timeout}")
    private int sessionTimeout;

    @Value("${password.iteration}")
    private Integer passwordIteration;

    @Value("${password.secret}")
    private String passwordSecret;

    @Value("${database.username}")
    private String dbUsername;

    @Value("${database.password}")
    private String dbPassword;

    @Value("${database.driver}")
    private String dbDriver;

    @Value("${database.dialect}")
    private String dbSqlDialect;

    @Value("${database.showSql}")
    private String dbShowSqlFlg;

    @Value("${database.strategy}")
    private String dbStrategy;

    @Value("${database.cache.use_second_level_cache}")
    private String dbCacheUseSecondLevel;

    @Value("${database.cache.use_query_cache}")
    private String dbCacheUseQuery;

    @Value("${database.cache.use_minimal_puts}")
    private String dbCacheUseMinPuts;

    @Value("${database.cache.region_prefix}")
    private String dbCacheRegionPrefix;

    @Value("${database.cache.provider_config_file_resource_path}")
    private String dbCacheProviderConfig;

    @Value("${database.cache.factory_class}")
    private String dbCacheFactoryClass;

    @Override
    public @NotNull String getApplicationVersion() {
        return Manifests.read("version");
    }

    @Override
    public @NotNull String getAuthorName() {
        return Manifests.read("author");
    }

    @Override
    public @NotNull String getAuthorEmail() {
        return Manifests.read("email");
    }

}
