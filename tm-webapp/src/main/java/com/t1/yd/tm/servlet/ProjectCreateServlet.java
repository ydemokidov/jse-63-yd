package com.t1.yd.tm.servlet;

import com.t1.yd.tm.repository.ProjectRepository;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/project/create/*")
public class ProjectCreateServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        ProjectRepository.INSTANCE.create();
        resp.sendRedirect("/projects");
    }

}
