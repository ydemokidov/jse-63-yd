<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/header.jsp"/>

<h3>PROJECT EDIT</h3>

<form action="/project/update?id=${project.id}" method="POST">
    <input type="hidden" name="id" value="${project.id}"/>
    <p>
    <div>Name:</div>
    <div><input type="text" name="name" value="${project.name}"/></div>
    </p>
    <p>
    <div>Description:</div>
    <div><input type="text" name="description" value="${project.description}"/></div>
    </p>
    <p>
    <div>Status:</div>
    <select name="status">
        <c:forEach var="status" items="${statusValues}">
            <option
                    <c:if test="${project.status == status}">selected="selected"</c:if>
                    value="${status}">${status.displayName}</option>
        </c:forEach>
    </select>
    </p>
    <p>
    <div>Created:</div>
    <div><input type="date" name="created" value="
        <fmt:parseDate value="${project.created}" pattern="yyyy-MM-dd" var="projectCreatedStr" type="date"/>
        <fmt:formatDate pattern="yyyy-MM-dd" value="${projectCreatedStr}"/>"/>
    </div>
    </p>
    <button type="submit" style="padding-top: 20px;">SAVE</button>
</form>

<jsp:include page="../include/footer.jsp"/>
