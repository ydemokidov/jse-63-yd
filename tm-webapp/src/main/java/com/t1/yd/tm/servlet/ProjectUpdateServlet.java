package com.t1.yd.tm.servlet;

import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.model.Project;
import com.t1.yd.tm.repository.ProjectRepository;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@WebServlet("/project/update/*")
public class ProjectUpdateServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final String id = req.getParameter("id");
        @Nullable final Project project = ProjectRepository.INSTANCE.findById(id);
        req.setAttribute("project", project);
        req.setAttribute("statusValues", Status.values());
        req.getRequestDispatcher("/WEB-INF/views/project-update.jsp").forward(req, resp);
    }

    @Override
    @SneakyThrows
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        @NotNull final String id = req.getParameter("id");
        @NotNull final String name = req.getParameter("name");
        @NotNull final String description = req.getParameter("description");
        @NotNull final String statusValue = req.getParameter("status");
        @NotNull final Status status = Status.valueOf(statusValue);
        @NotNull final String createdValue = req.getParameter("created");

        @NotNull final Project project = new Project();
        project.setId(id);
        project.setName(name);
        project.setDescription(description);
        project.setStatus(status);

        if (!createdValue.isEmpty())
            project.setCreated(LocalDate.parse(createdValue, DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        else project.setCreated(null);
        ProjectRepository.INSTANCE.save(project);
        resp.sendRedirect("/projects");
    }

}
