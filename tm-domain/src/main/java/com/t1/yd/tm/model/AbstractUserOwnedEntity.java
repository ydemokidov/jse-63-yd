package com.t1.yd.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
public abstract class AbstractUserOwnedEntity extends AbstractEntity {

    @Nullable
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public AbstractUserOwnedEntity(@Nullable User user) {
        this.user = user;
    }

}
