package com.t1.yd.tm.api.service.model;

import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IUserService extends IService<User> {

    @NotNull
    User create(@NotNull String login, @NotNull String password);

    @NotNull
    User create(@NotNull String login, @NotNull String password, @NotNull String email);

    @NotNull
    User create(@NotNull String login, @NotNull String password, @NotNull String email, @NotNull Role role);

    @NotNull
    User removeByLogin(@NotNull String login);

    @NotNull
    User removeByEmail(@NotNull String email);

    @NotNull
    User setPassword(@NotNull String id, @NotNull String password);

    @NotNull
    User updateUser(@NotNull String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findByEmail(@NotNull String email);

    void lockByLogin(@NotNull String login);

    void unlockByLogin(@NotNull String login);

    @NotNull
    Boolean isLoginExist(@NotNull String login);

    @NotNull
    Boolean isEmailExist(@NotNull String email);

}
