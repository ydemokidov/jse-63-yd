package com.t1.yd.tm.servlet;

import com.t1.yd.tm.model.Project;
import com.t1.yd.tm.repository.ProjectRepository;
import org.jetbrains.annotations.NotNull;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;

@WebServlet("/projects/*")
public class ProjectListServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        @NotNull final Collection<Project> projects = ProjectRepository.INSTANCE.findAll();
        req.setAttribute("projects", projects);
        req.getRequestDispatcher("/WEB-INF/views/project-list.jsp").forward(req, resp);
    }

}
