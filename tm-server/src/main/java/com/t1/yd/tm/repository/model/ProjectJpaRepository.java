package com.t1.yd.tm.repository.model;

import com.t1.yd.tm.model.Project;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

@Repository
@Scope("prototype")
public interface ProjectJpaRepository extends AbstractJpaUserOwnedRepository<Project> {

}
