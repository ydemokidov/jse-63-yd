package com.t1.yd.tm.repository.model;

import com.t1.yd.tm.model.Task;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Scope("prototype")
public interface TaskJpaRepository extends AbstractJpaUserOwnedRepository<Task> {

    List<Task> findAllByProjectId(String projectId);

    List<Task> findAllByUserIdAndProjectId(String userId, String projectId);

}
